﻿using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.TreeCrooks.Components.Builders {
    public class CrookComponentBuilder : IItemComponentBuilder {
        public string Kind() {
            return "crook";
        }

        public object Instance(BaseItemConfiguration item, Blob config) {
            return new CrookComponent(config);
        }
    }
}
