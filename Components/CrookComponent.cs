﻿using Plukit.Base;

namespace NimbusFox.TreeCrooks.Components {
    public class CrookComponent {
        public long Percentage { get; }
        public long Minimum { get; }
        public long Maximum { get; }
        public CrookComponent(Blob config) {
            Percentage = config.GetLong("percentage", 25);
            Minimum = config.GetLong("minimum", 0);
            Maximum = config.GetLong("maximum", 3);
        }
    }
}
