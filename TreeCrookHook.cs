﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NimbusFox.TreeCrooks.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.TreeCrooks {
    public class TreeCrookHook : IModHookV4 {

        public static TreeCrookHook Instance { get; private set; }

        public TreeCrookHook() {
            Instance = this;
        }

        private Dictionary<string, CrookConfiguration> _configurations { get; } = new Dictionary<string, CrookConfiguration>();

        public CrookConfiguration GetConfig(string code) {
            return !_configurations.ContainsKey(code) ? null : _configurations[code];
        }

        public IEnumerable<CrookConfiguration> AllConfigs => _configurations.Values.ToList();

        public CrookConfiguration GetConfig(TileConfiguration config) {
            return _configurations.Values.FirstOrDefault(x => x.Plant.Code == config.Code);
        }

        public void Dispose() {
            _configurations.Clear();
        }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }

        public void GameContextInitializeAfter() {
            _configurations.Clear();
            var crooks = GameContext.AssetBundleManager.FindByExtension(".crook");

            foreach (var crook in crooks) {
                using (var stream = GameContext.ContentLoader.ReadStream(crook)) {
                    stream.Seek(0, SeekOrigin.Begin);

                    var blob = BlobAllocator.Blob(true);

                    blob.LoadJsonStream(stream);

                    if (_configurations.ContainsKey(blob.GetString("code"))) {
                        throw new Exception(
                            $"Crook database already has a crook definition with the code '{blob.GetString("code")}'");
                    }

                    _configurations.Add(blob.GetString("code"), new CrookConfiguration(blob));

                    Blob.Deallocate(ref blob);
                }
            }

            Logger.WriteLine($"{_configurations.Count} Crook configurations loaded");
        }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
