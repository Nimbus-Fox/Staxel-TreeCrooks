﻿using Plukit.Base;
using Staxel;
using Staxel.Farming;
using Staxel.Tiles;

namespace NimbusFox.TreeCrooks.Classes {
    public class CrookConfiguration {
        public string Code { get; }
        public TileConfiguration Plant { get; }
        public PlantConfiguration Seeds { get; }
        public CrookConfiguration(Blob config) {
            Code = config.GetString("code");
            Plant = GameContext.TileDatabase.GetTileConfiguration(config.GetString("plant"));
            Seeds = GameContext.PlantDatabase.GetByTile(Plant);
        }
    }
}
