﻿using System.Linq;
using NimbusFox.TreeCrooks.Classes;
using NimbusFox.TreeCrooks.Components;
using NimbusFox.TreeCrooks.Items.Builders;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Entities;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Notifications;
using Staxel.Player;
using Staxel.Tiles;
using Staxel.Translation;

namespace NimbusFox.TreeCrooks.Items {
    public class CrookItem : Item {
        protected CrookItemBuilder Builder;
        protected CrookComponent Component;

        public CrookItem(CrookItemBuilder builder, ItemConfiguration configuration) : base(builder.Kind()) {
            Builder = builder;
            Configuration = configuration;

            var component = Configuration.Components.Select<CrookComponent>().FirstOrDefault();

            Component = component ?? new CrookComponent(BlobAllocator.Blob(true));
        }

        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) { }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (!main.DownClick) {
                return;
            }

            if (entity.Logic is PlayerEntityLogic playerLogic) {
                if (playerLogic.LookingAtTile(out var target, out _)) {
                    if (facade.FindReadCompoundTileCore(target, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var core, out var tile)) {
                        if (GameContext.PlantDatabase.IsPlant(tile)) {
                            if (GameContext.PlantDatabase.TryGetByTile(tile, out var plantConfig)) {
                                var crook = TreeCrookHook.Instance.AllConfigs.FirstOrDefault(x => x.Seeds.Code == plantConfig.Code);

                                if (crook == default(CrookConfiguration)) {
                                    return;
                                }

                                if (!crook.Seeds.IsSeedTile(tile) && !crook.Seeds.IsWiltedTile(tile) &&
                                    !crook.Seeds.IsWitheredTile(tile) &&
                                    (!crook.Seeds.GetGrowthStage(tile).BreakRemains.IsNullOrEmpty() ||
                                     crook.Seeds.GetGrowthStage(tile).CanBeGrownToPickableState && crook.Seeds.GetGrowthStage(tile) != crook.Seeds.FullyGrownStage)) {
                                    facade.RemoveTile(entity, core, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId);
                                    var plantTile = crook.Seeds.MakePlantedSeed(GameContext.RandomSource);
                                    facade.PlaceTile(entity, core, plantTile, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId);
                                    if (GameContext.RandomSource.NextLong(0, 100) <= Component.Percentage) {
                                        var qty = GameContext.RandomSource.NextLong(Component.Minimum, Component.Maximum);
                                        if (qty != 0) {
                                            var item = new ItemStack(crook.Seeds.MakeItem(), (int)qty);

                                            ItemEntityBuilder.SpawnDroppedItem(entity, facade, item,
                                                target.ToTileCenterVector3D(), Vector3D.Zero, Vector3D.Zero,
                                                SpawnDroppedFlags.AttemptPickup);
                                        }
                                    }
                                } else {
                                    var notification = GameContext.NotificationDatabase.CreateNotificationFromCode(
                                        "nimbusfox.treecrooks.notifications.cantcrook", facade.Step, new NotificationParams(), true);

                                    playerLogic.ShowNotification(notification);
                                }
                            }
                        }
                    }
                }
            }
        }

        public override bool Same(Item item) {
            return item.GetItemCode() == Configuration.Code;
        }

        public override bool TryResolveMainInteractVerb(Entity entity, EntityUniverseFacade facade, Vector3I location,
            TileConfiguration lookedAtTile, out string verb) {
            verb = "nimbusfox.treecrooks.verb.crook";

            return TreeCrookHook.Instance.AllConfigs.Any(x => x.Plant.Code == lookedAtTile.Code);
        }

        protected override void AssignFrom(Item item) { }
        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            return false;
        }

        public override bool HasAssociatedToolComponent(Plukit.Base.Components components) {
            return components.Select<CrookComponent>().Count() != 0;
        }

        public override ItemRenderer FetchRenderer() {
            return Builder.Renderer;
        }

        public override string GetItemDescription(LanguageDatabase lang) {
            return string.Format(lang.GetTranslationString("nimbusfox.treecrooks.item.crook.description"),
                Component.Percentage, Component.Minimum, Component.Maximum);
        }
    }
}
