﻿using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.TreeCrooks.Items.Builders {
    public class CrookItemBuilder : IItemBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }

        public void Load() {
            Renderer = new ItemRenderer();
        }

        public ItemRenderer Renderer;

        public Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            return new CrookItem(this, configuration);
        }

        public string Kind() {
            return "nimbusfox.treecrooks.item.crook";
        }
    }
}
